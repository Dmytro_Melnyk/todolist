// Your code goes here

let mainDiv = document.getElementById('root');

let title = document.createElement('h1');
title.innerHTML = 'TODO Cat List';
title.className = 'title';
mainDiv.append(title);

let inputDiv = document.createElement('div');
mainDiv.append(inputDiv);

let input = document.createElement('input');
input.className = 'input-text';
input.placeholder = 'add new action';
input.addEventListener('keyup', () => {
  btnAdd.disabled = !input.value;
});
inputDiv.append(input);

let iconBtnAdd = document.createElement('i');
iconBtnAdd.className = 'material-icons';
iconBtnAdd.textContent = 'add';

let btnAdd = document.createElement('button');
btnAdd.className = 'add-button';
btnAdd.disabled = true;
btnAdd.append(iconBtnAdd);
inputDiv.append(btnAdd);

let line = document.createElement('hr');
line.className = 'line';
mainDiv.append(line);

let actionsDiv = document.createElement('div');
actionsDiv.id = 'columns';
mainDiv.append(actionsDiv);

let listItems;

let dragSrcEl = null;

function handleDragStart(e) {
  this.classList.add('dragStartClass');
  dragSrcEl = this;
  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
}

function handleDragOver(e) {
  e.preventDefault();
  e.dataTransfer.dropEffect = 'move'; // sets cursor
  return false;
}

function handleDragEnter(e) {
  this.classList.add('over');
}

function handleDragLeave(e) {
  this.classList.remove('over');
}

function handleDrop(e) {
  let listItems = document.querySelectorAll('.div');
  e.stopPropagation(); // stops the browser from redirecting.
  let dragSrcOrderId = parseInt(dragSrcEl.getAttribute('order-id'));
  let dragTargetOrderId = parseInt(this.getAttribute('order-id'));
  console.log(dragSrcOrderId);
  console.log(dragTargetOrderId);
  // Don't do anything if dropping the same column we're dragging.
  // and
  // check if only one difference and then do not execute
  // && ((Math.abs(dragSrcOrderId - dragTargetOrderId)) != 1)
  if (dragSrcEl !== this) {
    // Set the source column's HTML to the HTML of the column we dropped on.
    let tempThis = this;

    function makeNewOrderIds(tempThis) {
      // check if up or down movement
      if (dragTargetOrderId !== dragSrcOrderId + 1) {
        dragSrcEl.setAttribute('order-id', dragTargetOrderId);
        tempThis.setAttribute('order-id', dragTargetOrderId);
      }
      //  find divs between old and new location and set new ids - different in up or down movement (if else)
      if (dragSrcOrderId < dragTargetOrderId) {
        for (let i = dragSrcOrderId + 1; i < dragTargetOrderId; i++) {
          listItems[i].setAttribute('order-id', i - 1);
          // set new id src
          dragSrcEl.setAttribute('order-id', dragTargetOrderId - 1);
        }
      } else {
        for (i = dragTargetOrderId; i < dragSrcOrderId; i++) {
          listItems[i].setAttribute('order-id', i + 1);
          // set new id src
          dragSrcEl.setAttribute('order-id', dragTargetOrderId);
        }
      }
    }
    makeNewOrderIds(tempThis);
    dragSrcEl.classList.remove('dragStartClass');
    reOrder(listItems);
  } else {
    dragSrcEl.classList.remove('dragStartClass');
    return false;
  }
}

function handleDragEnd(e) {
  for (let i = 0; i < listItems.length; i++) {
    let listItem = listItems[i];
    listItem.classList.remove('over');
  }
  dragSrcEl.classList.remove('dragStartClass');
}

function reOrder(listItems) {
  let tempListItems = listItems;
  tempListItems = Array.prototype.slice.call(tempListItems, 0);
  tempListItems.sort(function(a, b) {
    return a.getAttribute('order-id') - b.getAttribute('order-id');
  });

  let parent = document.getElementById('columns');
  parent.innerHTML = '';

  for (let i = 0, l = tempListItems.length; i < l; i++) {
    parent.appendChild(tempListItems[i]);
  }
}

class Action {

  constructor(name) {
    this.createAction(name);
  }

  createAction(name) {
    let actionDiv = document.createElement('div');
    actionDiv.draggable = true;
    actionDiv.className = 'div';
    actionDiv.hidden = false;
    actionsDiv.append(actionDiv);

    listItems = document.querySelectorAll('.div');

    for (let i = 0; i < listItems.length; i++) {
      let listItem = listItems[i];
      listItem.setAttribute('order-id', i);
      listItem.addEventListener('dragstart', handleDragStart, false);
      listItem.addEventListener('dragenter', handleDragEnter, false);
      listItem.addEventListener('dragover', handleDragOver, false);
      listItem.addEventListener('dragleave', handleDragLeave, false);
      listItem.addEventListener('drop', handleDrop, false);
      listItem.addEventListener('dragend', handleDragEnd, false);
    }

    let actionCheckbox = document.createElement('label');
    actionCheckbox.className = 'container';
    actionCheckbox.classList.add('checkbox');
    actionCheckbox.type = 'checkbox';
    actionDiv.append(actionCheckbox);

    let checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    actionCheckbox.append(checkbox);

    let checkmark = document.createElement('span');
    checkmark.className = 'checkmark';
    actionCheckbox.append(checkmark);
    checkbox.addEventListener('input', () => {
      checkbox.disabled = true;
    });

    let action = document.createElement('span');
    action.className = 'action';
    action.innerHTML = name;
    actionDiv.append(action);

    let iconBtnEdit = document.createElement('i');
    iconBtnEdit.className = 'material-icons';
    iconBtnEdit.textContent = 'edit';

    let actionEditBtn = document.createElement('button');
    actionEditBtn.className = 'button';
    actionEditBtn.append(iconBtnEdit);
    actionDiv.append(actionEditBtn);

    let iconBtnRemove = document.createElement('i');
    iconBtnRemove.className = 'material-icons';
    iconBtnRemove.textContent = 'delete';

    let actionRemoveBtn = document.createElement('button');
    actionRemoveBtn.className = 'remove-button';
    actionRemoveBtn.append(iconBtnRemove);
    actionDiv.append(actionRemoveBtn);

    actionEditBtn.addEventListener('click', () => this.editAction(action, actionDiv));
    actionRemoveBtn.addEventListener('click', () => this.deleteAction(actionDiv));
  }

  editAction(action, actionDiv) {
    actionDiv.hidden = true;
    let div = document.createElement('div');
    div.className = 'div';
    div.draggable = true;
    actionDiv.after(div);

    listItems = document.querySelectorAll('.div');

    for (let i = 0; i < listItems.length; i++) {
      let listItem = listItems[i];
      listItem.setAttribute('order-id', i);
      listItem.addEventListener('dragstart', handleDragStart, false);
      listItem.addEventListener('dragenter', handleDragEnter, false);
      listItem.addEventListener('dragover', handleDragOver, false);
      listItem.addEventListener('dragleave', handleDragLeave, false);
      listItem.addEventListener('drop', handleDrop, false);
      listItem.addEventListener('dragend', handleDragEnd, false);
    }

    let editText = document.createElement('input');
    editText.className = 'edit-text';
    editText.placeholder = 'new action name';
    console.log(action.innerHTML);
    editText.value = action.innerHTML;
    div.append(editText);

    let iconBtnRemove = document.createElement('i');
    iconBtnRemove.className = 'material-icons';
    iconBtnRemove.textContent = 'save';

    let saveBtn = document.createElement('button');
    saveBtn.className = 'button';
    saveBtn.append(iconBtnRemove);
    div.append(saveBtn);

    saveBtn.addEventListener('click', function() {
      action.innerHTML = editText.value;
      actionDiv.hidden = false;
      div.remove();
    })
  }

  deleteAction(actionDiv) {
    actionDiv.remove();
  }
}

btnAdd.onclick = function() {
  if (input.value) {
    new Action(input.value);
    input.value = '';
    btnAdd.disabled = true;
  }
};
